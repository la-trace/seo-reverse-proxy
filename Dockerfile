# build
FROM golang:1.12 as builder
WORKDIR /go/src/gitlab.com/la-trace/seo-reverse-proxy
ADD . /go/src/gitlab.com/la-trace/seo-reverse-proxy/
RUN CGO_ENABLED=0 GOOS=linux go build -o seo-reverse-proxy

# exec
FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root
COPY --from=builder /go/src/gitlab.com/la-trace/seo-reverse-proxy .
CMD ["./seo-reverse-proxy"]