package main

import (
	"io"
	"log"
	"net/http"
	"os"
)

type fileResponseWriter struct {
	filepath string
	file     io.Writer
	resp     http.ResponseWriter
	status   int
}

func newFileResponseWriter(filepath string, resp http.ResponseWriter) http.ResponseWriter {
	// multi := io.MultiWriter(file, resp)
	return &fileResponseWriter{
		filepath: filepath,
		resp:     resp,
	}
}

// implement http.ResponseWriter
// https://golang.org/pkg/net/http/#ResponseWriter
func (w *fileResponseWriter) Header() http.Header {
	return w.resp.Header()
}

func (w *fileResponseWriter) Write(b []byte) (int, error) {
	// only creating cache if we get a 200 response from the target host
	if w.status == http.StatusOK {
		f, err := os.Create(w.filepath)
		if err != nil {
			return 0, err
		}
		defer f.Close()
		multi := io.MultiWriter(f, w.resp)
		return multi.Write(b)
	}
	// not a 200 response, only serving the http.ResponseWriter
	log.Print("Writing on response")
	return w.resp.Write(b)
}

func (w *fileResponseWriter) WriteHeader(i int) {
	// capturing the status of the request
	w.status = i
	w.resp.WriteHeader(i)
}
