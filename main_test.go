package main

import (
	"testing"
)

func Test_computeSha1(t *testing.T) {
	type args struct {
		content []byte
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "Main Test",
			args: args{
				content: []byte("foo"),
			},
			want: "0beec7b5ea3f0fdbc95d0dd47f3c5bc275da8a33",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := computeSha1(tt.args.content); got != tt.want {
				t.Errorf("computeSha1() = %v, want %v", got, tt.want)
			}
		})
	}
}
