package main

import (
	"crypto/sha1"
	"encoding/hex"
	"errors"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"time"
)

// Dead simple reverse proxy for SEO purpose popon top on of a rendertron host
// It will cache response as a file in a given folder
// Only the standard library needed here

var errorFileIsTooOld = errors.New("Cache File is too old")

type configuration struct {
	renderURL      string
	targetHost     string
	rendertronHost string
	cachePath      string
	port           string
	refresh        time.Duration
}

func getEnvOrPanic(name string) string {
	value := os.Getenv(name)
	if value == "" {
		log.Panicf("Env var %s must be set", name)
	}
	return value
}

func newConfigurationFromFromEnv() (*configuration, error) {
	conf := configuration{}
	conf.renderURL = getEnvOrPanic("RENDER_URL")
	conf.targetHost = getEnvOrPanic("TARGET_HOST")
	conf.rendertronHost = getEnvOrPanic("RENDERTRON_HOST")
	conf.cachePath = getEnvOrPanic("CACHE_PATH")
	conf.port = getEnvOrPanic("PORT")
	conf.refresh = time.Minute * 60 * 24 * 30
	return &conf, nil
}

func main() {
	// Populate configuration struct from env vars
	conf, err := newConfigurationFromFromEnv()
	if err != nil {
		panic(err)
	}

	log.Printf("Configuation Loaded: %+v", conf)

	// Creating our Reverse Proxy
	remote, err := url.Parse(conf.rendertronHost)
	if err != nil {
		panic(err)
	}
	proxy := httputil.NewSingleHostReverseProxy(remote)
	proxy.Transport = &myTransport{
		configuration: conf,
	}

	// Start the HTTP server
	http.HandleFunc("/", handler(proxy))
	err = http.ListenAndServe(conf.port, nil)
	if err != nil {
		panic(err)
	}
}

func handler(p *httputil.ReverseProxy) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		p.ServeHTTP(w, r)
	}
}

func loadCache(filepath string, refresh time.Duration) (*os.File, error) {
	// opening file
	f, err := os.Open(filepath)

	if err != nil {
		return nil, err
	}

	// get stats
	fileinfo, err := os.Stat(filepath)

	if err != nil {
		return nil, err
	}

	oldness := time.Since(fileinfo.ModTime())

	// test if file is too old
	if oldness > refresh {
		os.Remove(filepath)
		return nil, errorFileIsTooOld
	}

	return f, nil
}

func computeSha1(content []byte) string {
	h := sha1.New()
	h.Write(content)
	return hex.EncodeToString(h.Sum(nil))
}
