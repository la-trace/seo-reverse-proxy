package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

type myTransport struct {
	configuration *configuration
}

func (t *myTransport) RoundTrip(r *http.Request) (*http.Response, error) {
	// Work in plain text
	r.Header.Set("Accept-Encoding", "identity")
	hash := computeSha1([]byte(r.URL.Path))

	// Filepath
	filepath := t.configuration.cachePath + "/" + hash

	cacheControl := r.Header.Get("Cache-Control")

	if cacheControl != "no-cache" {
		// Load cache
		rf, err := loadCache(filepath, t.configuration.refresh)

		if err == errorFileIsTooOld {
			// Cache file is too old
			log.Printf("Cache file is too old for request: %s\n", r.URL.Path)
		} else if err == nil {
			// Cache file exists
			content, err := ioutil.ReadAll(rf)
			if err == nil {
				log.Printf("Serving request %s from cache", r.URL.Path)
				buf := new(bytes.Buffer)
				buf.ReadFrom(rf)

				return &http.Response{
					Status:        "200 OK",
					StatusCode:    http.StatusOK,
					Proto:         "HTTP/1.1",
					ProtoMajor:    1,
					ProtoMinor:    1,
					Body:          ioutil.NopCloser(bytes.NewReader(content)),
					ContentLength: int64(len(content)),
					Request:       r,
					Header:        make(http.Header, 0),
				}, nil
			}
			defer rf.Close()
		}
	} else {
		log.Println("Refresh Cache asked")
	}

	log.Printf("Serving request %s from backend", r.URL.Path)
	r.URL.Path = "/" + t.configuration.renderURL + "/" + t.configuration.targetHost + r.URL.Path

	response, err := http.DefaultTransport.RoundTrip(r)
	if err != nil {
		return nil, err //Server is not reachable. Server not working
	}

	buf, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	response.Body = ioutil.NopCloser(bytes.NewBuffer(buf))

	if response.StatusCode == http.StatusOK {
		f, err := os.Create(filepath)
		if err != nil {
			return nil, err
		}
		defer f.Close()
		f.Write(buf)
	}

	return response, err
}
